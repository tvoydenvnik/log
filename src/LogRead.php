<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 20.08.14
 * Time: 20:59
 */

namespace Tvoydenvnik\Log;


/**
 * Считывает строки с лог-файла, с запоминанием последней строки
 * Class zLogRead
 * @package lib\ServerLog
 */
class LogRead {
    /**
     * Путь к логу
     *  @var string
     */
    private $logPath = '';
    /**
     * Путь к файлу, где лежит информация об логе.
     * Содержит:
     *  Первую строку - по ней можно определить новый ли это лог
     *  Смещение последнего чтения
     * @var string
     */
    private $seekFilePath = '';

    /**
     * Ссылка на лог
     */
    private $handleLog;

    private $firstLineOfLog = '';


    public function __construct($pLogPath ,  $pTempPath = '/tmp/logRead/'){
        $this->logPath = $pLogPath;

        if(!is_dir($pTempPath)){
            mkdir($pTempPath, 0644);
        }
        $this->seekFilePath = $pTempPath . md5($pLogPath) . '__'. basename($this->logPath);

    }

    public function open(){
        $this->handleLog = fopen($this->logPath, 'r');

        if($this->handleLog!==false){
            fseek($this->handleLog, 0);

            $lFirstLine = fgets($this->handleLog, 4096);
            if($lFirstLine === false){
                $lFirstLine = '';
            }else{
                $lFirstLine = rtrim($lFirstLine, "\n");
            }

            $this->firstLineOfLog = $lFirstLine;
            //возращаем указатель в начало
            fseek($this->handleLog, 0);
        }
        return ($this->handleLog===false?false:true);
    }

    public function next(){
        if(feof($this->handleLog)===true){
            return false;
        }else{
            $line = fgets($this->handleLog, 4096);//str_replace("\n", '', fgets($this->handleLog)); fgets($this->handleLog) fgets($this->handleLog, 4096)
            if($line === "\n"){
                //Когда записываем смещение, смещение сохраняется на последней читаемой строке, но до символа переноса строк
                $line = fgets($this->handleLog, 4096);
                if(feof($this->handleLog)===true){
                    return false;
                }
            }
            if($line===false){
                return false;
            }
            return rtrim($line,"\n");
        }
    }

    /**
     * Перемещаем указатель на ранее читаемую строку: если возможно
     */
    public function seekToLastRead(){

        if(file_exists($this->seekFilePath) == false){
            //считываем сначала лога
            return false;
        }

        $lData = file_get_contents($this->seekFilePath);

        try{
            $lData = json_decode($lData, true);
        }catch (\Exception $e){
            $lData = array();
        }


        if(isset($lData['seekFilePath'])===false || isset($lData['firstLine'])===false || isset($lData['filesize'])===false){
            return false;
        }

        $currentFileSize = filesize($this->logPath);

        if($this->firstLineOfLog != $lData['firstLine'] || $currentFileSize < $lData['filesize']){
            //это новый лог
            fseek($this->handleLog, 0);
            return false;
        }else{
            $lResult = fseek($this->handleLog, intval($lData['seekFilePath']));
            return true;
        }
    }
    public function close($pSaveSeek = true){

        if($pSaveSeek == true){
            $info = array(
                "firstLine"=>$this->firstLineOfLog,
                "seekFilePath"=>ftell($this->handleLog),
                "filesize"=>filesize($this->logPath)
            );
            $text = json_encode($info);
            $file = fopen($this->seekFilePath, 'w');
            if(fwrite($file, $text)===false){
                return false;
            }
        }


        fclose($this->handleLog);

        return true;

    }
} 