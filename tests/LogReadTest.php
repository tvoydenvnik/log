<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 05.12.2014
 * Time: 14:33
 */

namespace  Tvoydenvnik\Log\Tests;



use Tvoydenvnik\Log\LogRead;

class LogReadTest extends \PHPUnit_Framework_TestCase {


//    protected function setUp()
//    {
//        $this->setBrowser('firefox');
//        $this->setBrowserUrl('http://192.168.81.129/');
//    }
//
//    public function testTitle()
//    {
//        $this->url('http://www.health-diet.ru/');
//        $this->assertEquals('Example WWW Page', $this->title());
//    }

    public function testForTest()
    {
        $this->assertEquals(true, true);

    }

    public static function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            //throw new InvalidArgumentException("$dirPath must be a directory");
            return;
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    public function testLogRead()
    {

        $dir = __DIR__ . '/tmp/';






        $lPathLog = __DIR__ .'/tmp/_testLog1.txt';

        $log = new LogRead($lPathLog, $dir);

        file_put_contents($lPathLog, '');
        $lText = file_get_contents($lPathLog);


        /*`
         * Читаем пустой лог
         */


        $this->assertEquals($log->open(), true, 'Открываем лог');
        $this->assertEquals($log->next(), false, 'Лог должен быть пустым');
        $this->assertEquals($log->close(false), true);

        ///////////////////////
        $lNewLog = 'firstLine'."\n".'secondLine';
        $g = file_put_contents($lPathLog, $lNewLog);
        $lText = file_get_contents($lPathLog);
        /////////////////////////

        /**
         * Читаем все строчки
         */
        $this->assertEquals($log->open(), true, 'Открываем лог');
        $this->assertEquals($log->next(), 'firstLine');
        $this->assertEquals($log->next(), 'secondLine');
        $this->assertEquals($log->next(), false);

        $this->assertEquals($log->close(), true);


        /**
         * Новых строчек нет
         */
        $log = new LogRead($lPathLog, $dir);
        $this->assertEquals($log->open(), true);
        $lResult = $log->seekToLastRead();
        $this->assertEquals($lResult, true);
        $this->assertEquals($log->next(), false);
        $this->assertEquals($log->close(), true);


        ///////////////////////
        $lNewLog = 'firstLine'."\n".'secondLine'."\n".'3Line'."\n".'4Line'."\n";
        $g = file_put_contents($lPathLog, $lNewLog);
        $lText = file_get_contents($lPathLog);
        /////////////////////////

        /**
         * Считываем новые две строчки
         */
        $log = new LogRead($lPathLog, $dir);
        $this->assertEquals($log->open(), true);
        $this->assertEquals($log->next(), 'firstLine');
        $this->assertEquals($log->seekToLastRead(), true);

        $this->assertEquals($log->next(), '3Line');
        $this->assertEquals($log->next(), '4Line');
        $this->assertEquals($log->next(), false);
        $this->assertEquals($log->close(), true);


        ///////////////////////
        $lNewLog = 'firstLine1'."\n".'secondLine'."\n".'3Line'."\n".'4Line';
        $g = file_put_contents($lPathLog, $lNewLog);
        $lText = file_get_contents($lPathLog);
        /////////////////////////

        /**
         * Это уже новый лог: изменилась первая строка
         */
        $log = new LogRead($lPathLog, $dir);
        $this->assertEquals($log->open(), true);
        $this->assertEquals($log->seekToLastRead(), false);
        $this->assertEquals($log->close(false), true);

        ///////////////////////
        $lNewLog = 'firstLine1'."\n".'seco'."\n".'3Line'."\n".'4Line';
        $g = file_put_contents($lPathLog, $lNewLog);
        $lText = file_get_contents($lPathLog);
        /////////////////////////

        /**
         * Это уже новый лог: т.к. уменьшился размер
         */
        $this->assertEquals($log->open(), true);
        $this->assertEquals($log->next(), 'firstLine1');
        $this->assertEquals($log->seekToLastRead(), false);
        $this->assertEquals($log->close(false), true);




        self::deleteDir($dir);
    }



}
 